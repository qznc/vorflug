/**
OptParse is a command line argument parser

It is inspired by Python's OptParse.
The arguments -h --help -v --version are automatically initialized.
Additional short and long arguments may be added and are automatically included in the Usage output.
*/

#ifndef OPTPARSE_H
#define OPTPARSE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

typedef struct _parse_option parse_option;

typedef struct optparse {
		const char * version;
		parse_option *last;
		const char * progname;
		int after_argc;
		char **after_argv;
} optparse;

void optparse_init(optparse * const parser, const char * const progversion);

void optparse_add_bool_option(optparse *parser, const char letter, const char *const word, const bool *store, const char* const description);
void optparse_add_int_option(optparse *parser, const char letter, const char *const word, const int *store, const char* const description);
void optparse_add_string_option(optparse *parser, const char letter, const char *const word, char **const store, const char* const description);

void optparse_parse(optparse *parser, int argc, char *argv[], char **after_argv[]);

#ifdef __cplusplus
}
#endif

#endif

// vim: noexpandtab
