EXT_LDFLAGS=$(shell pkg-config --libs poppler)
EXT_CFLAGS=$(shell pkg-config --cflags poppler)

DESTDIR?=/usr/local

CFLAGS+=-Wall -pedantic -O3

all: vorflug

vorflug.o: vorflug.cc optparse.o
	$(CXX) -std=c++11 $(CFLAGS) $(EXT_CFLAGS) $< -c -o $@

optparse.o: optparse.c optparse.h
	$(CC) -std=c99 $(CFLAGS) $< -c -o $@

vorflug: vorflug.o optparse.o
	$(CXX) $^ $(EXT_LDFLAGS) -o $@

.PHONY: clean distclean install

clean:
	rm -f *.o

distclean: clean
	rm -f vorflug

install: vorflug
	install -D -s vorflug $(DESTDIR)/bin/vorflug
