#include "optparse.h"

#include "string.h"
#include "stdlib.h"
#include "stdio.h"

typedef enum parse_option_type {
	ot_bool,
	ot_int,
	ot_string,
	ot_end_of_enum
} parse_option_type;

struct _parse_option {
	parse_option_type typ;
	char letter;
	const char *word;
	const void *store;
	void (*callback)(optparse *parser);
	const char* description;
	parse_option *prev;
};

static void version_and_exit(optparse *parser) {
	printf("%s %s\n", parser->progname, parser->version);
	exit(0);
}
static parse_option version_option =
	{ot_bool, 'V', "version", NULL, version_and_exit, "Show program version", NULL};

static void usage_and_exit(optparse *parser) {
	printf("Usage: %s [OPTION...]\n\n", parser->progname);
	parse_option *opt = parser->last;
	while (NULL != opt) {
		if (opt->letter)
			printf("-%c ", opt->letter);
		if (opt->word)
			printf("--%-10s ",opt->word);
		printf("\t%s\n", opt->description);
		opt = opt->prev;
	}
	exit(0);
}
static parse_option help_option =
	{ot_bool, 'h', "help", NULL, usage_and_exit, "Show this help", &version_option};

/** returns whether the next argument was consumed */
static bool process(optparse *parser, parse_option *opt, char next[])
{
	if (opt->callback != NULL)
		opt->callback(parser);
	switch (opt->typ) {
		case ot_bool:
			if (opt->store != NULL)
				*((bool*) (opt->store)) = true;
			return false;
		case ot_int:
			if (opt->store != NULL)
				*((int*) (opt->store)) = atoi(next);
			return true;
		case ot_string:
			if (opt->store != NULL)
				*((char**) (opt->store)) = next;
			return true;
		default:
			break;
	}
	return false;
}

void optparse_init(optparse * const parser, const char * const progversion)
{
	parser->version = progversion;
	parser->last = &help_option;
}


static parse_option *init_parse_option(const char letter, const char *const word, const void *store, const char* const description)
{
	parse_option *opt = (parse_option*) malloc(sizeof(parse_option));
	opt->letter = letter;
	opt->word = word;
	opt->store = store;
	opt->description = description;
	return opt;
}

void optparse_add_bool_option(optparse *parser, const char letter, const char *const word, const bool *store, const char* const description)
{
	parse_option *opt = init_parse_option(letter, word, store, description);
	opt->typ = ot_bool;
	opt->prev = parser->last;
	parser->last = opt;
}

void optparse_add_int_option(optparse *parser, const char letter, const char *const word, const int *store, const char* const description)
{
	parse_option *opt = init_parse_option(letter, word, store, description);
	opt->typ = ot_int;
	opt->prev = parser->last;
	parser->last = opt;
}

void optparse_add_string_option(optparse *parser, const char letter, const char *const word, char **const store, const char* const description)
{
	parse_option *opt = init_parse_option(letter, word, store, description);
	opt->typ = ot_string;
	opt->prev = parser->last;
	parser->last = opt;
}

static bool parseShort(optparse *parser, char arg[], char next[])
{
	parse_option *opt = parser->last;
	while (NULL != opt) {
		if (opt->letter == arg[0]) {
			return process(parser, opt, next);
		}
		opt = opt->prev;
	}
	return false;
}

static bool parseLong(optparse *parser, char arg[], char next[])
{
	parse_option *opt = parser->last;
	while (NULL != opt) {
		if (strcmp(opt->word,arg) == 0) {
			return process(parser, opt, next);
		}
		opt = opt->prev;
	}
	return false;
}

void optparse_parse(optparse *parser, int argc, char *argv[], char **after_argv[])
{
	parser->progname = (*after_argv)[0] = argv[0];
	int j=1;
	for(int i=1; i<argc; ++i) {
		char *arg = argv[i];
		char *next = argv[i+1];
		if (i==argc) next = NULL;
		if (arg[0] != '-') { /* argument */
			(*after_argv)[j] = arg;
			j++;
		} else if (arg[1] == '-') { /* long option */
			if (parseLong(parser, arg+2, next)) i++;
		} else { /* short option */
			if (parseShort(parser, arg+1, next)) i++;
		}
	}
}

// vim: noexpandtab
