A command line preflight tool

Preflight means to check pdf files for print requirements.
There are commercial applications available
(e.g. Markzware FlightCheck, Preflight in Adobe InDesign, or Job Jacket in Quark XPress),
but this is the cheap Free Software / Open Source  version.

## Features

* check that all fonts are embedded
* check that no strokes, fills, or images use RGB colors
* check for sufficient image resolution

## TODO

* CMYK colors everywhere?
* are all images embedded?
* sufficient width (no hair line width)
* which image formats?
* color profiles?
* borders, margins, etc?
* not too much color application?

http://markzware.com/preflight-software/pdf-preflight-problems-same-enfocus-survey/2009/06/26/
http://www.digital-engineer.net/archive/entry/preflighters-around-the-world-this-is-you/
http://www.prepressure.com/pdf/basics/preflight

Not direct checks, but also desired features:

* better error messages (position, etc)
* Preflight profiles?

## Licence

Due to the use of libpoppler, this software is licensed as GPLv2 or later.
See COPYING for the full licence text.
