#include <poppler-config.h>

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <math.h>

#include <goo/gtypes.h>
#include <goo/GooString.h>
#include <Object.h>
#include <OutputDev.h>
#include <GfxState.h>
#include <GfxFont.h>
#include <PDFDoc.h>
#include <PDFDocFactory.h>
#include <FontInfo.h>
#include <GlobalParams.h>

#include "optparse.h"

static const char * const VERSION = "0.0";

static const char * const fontTypeNames[] = {
	"unknown",
	"Type 1",
	"Type 1C",
	"Type 1C (OT)",
	"Type 3",
	"TrueType",
	"TrueType (OT)",
	"CID Type 0",
	"CID Type 0C",
	"CID Type 0C (OT)",
	"CID TrueType",
	"CID TrueType (OT)"
};

/* assert: size of enum GfxColorSpaceMode */
static const unsigned nGfxColorSpaceModes = 11;

static double distance(double x, double y)
{
	return sqrt(x*x + y*y);
}

static void stderr_printfln(FILE* channel, const char* prefix, const char* format, va_list args)
{
	fprintf(channel, "%s: ", prefix);
	vfprintf(channel, format, args);
	fprintf(channel, "\n");
}

static void error(const char* format, ...)
{
	va_list args;
	va_start(args, format);
	stderr_printfln(stderr, "error", format, args);
	va_end(args);
}

/** fail is some preflight check, which fails */
static void fail(const char* format, ...)
{
	va_list args;
	va_start(args, format);
	stderr_printfln(stdout, "fail", format, args);
	va_end(args);
}

class ValidatingOutputDev : public OutputDev {
	public:
		ValidatingOutputDev() {
			non_cmyk_stream = 0;
			pageNum = 0;
			for(unsigned i=0; i < nGfxColorSpaceModes; ++i) {
				/* "csm" for ColorSpaceMode ;) */
				fill_csm_counts[i] = 0;
				stroke_csm_counts[i] = 0;
				image_csm_counts[i] = 0;
			}
		}

		virtual ~ValidatingOutputDev() { }

		void printReport() {
			if (non_cmyk_stream > 0) {
				fail("Found %d non-CMYK streams!", non_cmyk_stream);
			}
			// TODO only warn for RGB? CMYK is fine, but Indexed, ICC, ...?
			unsigned rgb_strokes = stroke_csm_counts[csDeviceRGB] + stroke_csm_counts[csCalRGB];
			if (rgb_strokes > 0) {
				fail("Detected %d RGB stroke colors", rgb_strokes);
			}
			unsigned rgb_fills = fill_csm_counts[csDeviceRGB] + fill_csm_counts[csCalRGB];
			if (rgb_fills > 0) {
				fail("Detected %d RGB fill colors", rgb_fills);
			}
			unsigned rgb_images = image_csm_counts[csDeviceRGB] + image_csm_counts[csCalRGB];
			if (rgb_images > 0) {
				fail("Contains RGB images: %d", rgb_images);
			}
		}

		virtual void startPage(int pageNum, GfxState *state) { assert(false); }
		virtual void endPage() { }
		virtual void setPageNum(int num) { this->pageNum = num; }

		virtual GBool upsideDown() { return gFalse; }
		virtual GBool useDrawChar() { return gTrue; }
		virtual GBool interpretType3Chars() { return gTrue; }

		//----- save/restore graphics state
		virtual void saveState(GfxState * state) { checkGfxState(state); }
		virtual void restoreState(GfxState * state) { checkGfxState(state); }

		//----- update graphics state
		virtual void updateAll(GfxState *state) { checkGfxState(state); }
		virtual void setDefaultCTM(double *ctm) { }
		virtual void updateCTM(GfxState * state, double m11, double m12,
				double m21, double m22, double m31, double m32) { checkGfxState(state); }
		virtual void updateLineDash(GfxState * state) { checkGfxState(state); }
		virtual void updateFlatness(GfxState * state) { checkGfxState(state); }
		virtual void updateLineJoin(GfxState * state) { checkGfxState(state); }
		virtual void updateLineCap(GfxState * state) { checkGfxState(state); }
		virtual void updateMiterLimit(GfxState * state) { checkGfxState(state); }
		virtual void updateLineWidth(GfxState * state) { checkGfxState(state); }
		virtual void updateStrokeAdjust(GfxState * state) { checkGfxState(state); }
		virtual void updateAlphaIsShape(GfxState * state) { checkGfxState(state); }
		virtual void updateTextKnockout(GfxState * state) { checkGfxState(state); }
		virtual void updateFillColorSpace(GfxState * state) { checkGfxState(state); }
		virtual void updateStrokeColorSpace(GfxState * state) { checkGfxState(state); }
		virtual void updateFillColor(GfxState * state) { checkGfxState(state); }
		virtual void updateStrokeColor(GfxState * state) { checkGfxState(state); }
		virtual void updateBlendMode(GfxState * state) { checkGfxState(state); }
		virtual void updateFillOpacity(GfxState * state) { checkGfxState(state); }
		virtual void updateStrokeOpacity(GfxState * state) { checkGfxState(state); }
		virtual void updateFillOverprint(GfxState * state) { checkGfxState(state); }
		virtual void updateStrokeOverprint(GfxState * state) { checkGfxState(state); }
		virtual void updateOverprintMode(GfxState * state) { checkGfxState(state); }
		virtual void updateTransfer(GfxState * state) { checkGfxState(state); }
		virtual void updateFillColorStop(GfxState * state, double offset) { checkGfxState(state); }

		//----- update text state
		virtual void updateFont(GfxState * state) { checkGfxState(state); }
		virtual void updateTextMat(GfxState * state) { checkGfxState(state); }
		virtual void updateCharSpace(GfxState * state) { checkGfxState(state); }
		virtual void updateRender(GfxState * state) { checkGfxState(state); }
		virtual void updateRise(GfxState * state) { checkGfxState(state); }
		virtual void updateWordSpace(GfxState * state) { checkGfxState(state); }
		virtual void updateHorizScaling(GfxState * state) { checkGfxState(state); }
		virtual void updateTextPos(GfxState * state) { checkGfxState(state); }
		virtual void updateTextShift(GfxState * state, double shift) { checkGfxState(state); }

		//----- path painting
		virtual void stroke(GfxState * state) { checkGfxState(state); }
		virtual void fill(GfxState * state) { checkGfxState(state); }
		virtual void eoFill(GfxState * state) { checkGfxState(state); }
		virtual GBool tilingPatternFill(GfxState * state, Catalog * cat, Object * str,
				double * pmat, int paintType, Dict * resDict,
				double * mat, double * bbox,
				int x0, int y0, int x1, int y1,
				double xStep, double yStep)
		{ checkGfxState(state); return gFalse; }
		virtual GBool functionShadedFill(GfxState * state,
				GfxFunctionShading * shading)
		{ checkGfxState(state); return gFalse; }
		virtual GBool axialShadedFill(GfxState * state, GfxAxialShading * shading, double tMin, double tMax)
		{ checkGfxState(state); return gFalse; }
		virtual GBool axialShadedSupportExtend(GfxState * state, GfxAxialShading * shading)
		{ checkGfxState(state); return gFalse; }
		virtual GBool radialShadedFill(GfxState * state, GfxRadialShading * shading, double sMin, double sMax)
		{ checkGfxState(state); return gFalse; }
		virtual GBool radialShadedSupportExtend(GfxState * state, GfxRadialShading * shading)
		{ checkGfxState(state); return gFalse; }
		virtual GBool gouraudTriangleShadedFill(GfxState *state, GfxGouraudTriangleShading *shading)
		{ checkGfxState(state); return gFalse; }
		virtual GBool patchMeshShadedFill(GfxState *state, GfxPatchMeshShading *shading)
		{ checkGfxState(state); return gFalse; }

		//----- path clipping
		virtual void clip(GfxState * state) { checkGfxState(state); }
		virtual void eoClip(GfxState * state) { checkGfxState(state); }
		virtual void clipToStrokePath(GfxState * state) { checkGfxState(state); }

		//----- text drawing
		virtual void beginStringOp(GfxState * state) { checkGfxState(state); }
		virtual void endStringOp(GfxState * state) { checkGfxState(state); }
		virtual void beginString(GfxState * state, GooString * s) { checkGfxState(state); }
		virtual void endString(GfxState * state) { checkGfxState(state); }
		virtual void drawChar(GfxState * state, double x, double y,
				double dx, double dy,
				double originX, double originY,
				CharCode code, int nBytes, Unicode * u, int uLen) { checkGfxState(state); }
		virtual void drawString(GfxState * state, GooString * s) { checkGfxState(state); }
		virtual GBool beginType3Char(GfxState * state, double x, double y,
				double dx, double dy,
				CharCode code, Unicode * u, int uLen) { checkGfxState(state); return gTrue; }
		virtual void endType3Char(GfxState * state) { checkGfxState(state); }
		virtual void beginTextObject(GfxState * state) { checkGfxState(state); }
		virtual GBool deviceHasTextClip(GfxState * state) { return gFalse; }
		virtual void endTextObject(GfxState * state) { checkGfxState(state); }

		//----- grouping operators↵
		virtual void beginMarkedContent(char *name, Dict *properties) { }
		virtual void endMarkedContent(GfxState *state) { }
		virtual void markPoint(char *name) { }
		virtual void markPoint(char *name, Dict *properties) { }

		//----- image drawing
		virtual void drawImageMask(GfxState *state, Object *ref, Stream *str, int width, int height, GBool invert, GBool interpolate, GBool inlineImg)
		{
			checkResolution(width, height, state);
			checkGfxState(state);
			checkStream(str);
		}

		//----- more methods for linker
		virtual void cvtDevToUser(double dx, double dy, double *ux, double *uy) { }
		virtual void cvtUserToDev(double ux, double uy, int *dx, int *dy) { }
		virtual void startProfile() { }
		virtual GooHash *endProfile() { return NULL; }

		virtual void drawImage(GfxState *state, Object *ref, Stream *str,
				int width, int height, GfxImageColorMap *colorMap,
				GBool interpolate, int *maskColors, GBool inlineImg)
		{
			checkResolution(width, height, state);
			checkGfxState(state);
			checkStream(str);
			checkGfxImageColorMap(colorMap);
		}

		virtual void drawMaskedImage(GfxState *state, Object *ref, Stream *str, int width, int height, GfxImageColorMap *colorMap, GBool interpolate, Stream *maskStr, int maskWidth, int maskHeight, GBool maskInvert, GBool maskInterpolate)
		{
			checkResolution(width, height, state);
			checkGfxState(state);
			checkStream(str);
			checkGfxImageColorMap(colorMap);
		}

		virtual void drawSoftMaskedImage(GfxState *state, Object *ref, Stream *str, int width, int height, GfxImageColorMap *colorMap, GBool interpolate, Stream *maskStr, int maskWidth, int maskHeight, GfxImageColorMap *maskColorMap, GBool maskInterpolate)
		{
			checkResolution(width, height, state);
			checkGfxState(state);
			checkStream(str);
			checkGfxImageColorMap(colorMap);
		}

#if OPI_SUPPORT
		//----- OPI functions
		virtual void opiBegin(GfxState *state, Dict *opiDict) { checkGfxState(state); }
		virtual void opiEnd(GfxState *state, Dict *opiDict) { checkGfxState(state); }
#endif

		//----- Type 3 font operators
		virtual void type3D0(GfxState * state, double wx, double wy) { checkGfxState(state); }
		virtual void type3D1(GfxState * state, double wx, double wy,
				double llx, double lly, double urx, double ury) { checkGfxState(state); }

		//----- transparency groups and soft masks
		virtual void beginTransparencyGroup(GfxState * state, double * bbox,
				GfxColorSpace * blendingColorSpace,
				GBool isolated, GBool knockout,
				GBool forSoftMask) { checkGfxState(state); }
		virtual void endTransparencyGroup(GfxState * state) { checkGfxState(state); }
		virtual void paintTransparencyGroup(GfxState * state, double * bbox) { checkGfxState(state); }
		virtual void setSoftMask(GfxState * state, double * bbox, GBool alpha,
				Function * transferFunc, GfxColor * backdropColor) { checkGfxState(state); }
		virtual void clearSoftMask(GfxState * state) { checkGfxState(state); }

	private:
		int pageNum;
		unsigned non_cmyk_stream;
		unsigned stroke_csm_counts[nGfxColorSpaceModes];
		unsigned fill_csm_counts[nGfxColorSpaceModes];
		unsigned image_csm_counts[nGfxColorSpaceModes];

		void checkResolution(int width, int height, GfxState *state) {
			/* PDf 1.7 Spec says "All images are 1 unit wide by 1 unit high
			 * in user space, regardless of the number of samples in the image." */

			/* top left */
			double device_tlx, device_tly;
			state->transform(0.0,0.0,&device_tlx,&device_tly);
			/* top right */
			double device_trx, device_try;
			state->transform(1.0,0.0,&device_trx,&device_try);
			/* bottom left */
			double device_blx, device_bly;
			state->transform(0.0,1.0,&device_blx,&device_bly);
			/* transformed width and height */
			double device_width  = distance(device_tlx-device_trx, device_tly-device_try);
			double device_height = distance(device_tlx-device_blx, device_tly-device_bly);
			if (device_width > width || device_height > height) {
				fail("Low resolution image on page %d: %dx%d -scaled-> %.0fx%.0f", pageNum, width, height, device_width, device_height);
			}
		}

		void checkStream(Stream *str) {
			int *bitsPerComponent = 0;
			StreamColorSpaceMode *csMode = NULL;
			str->getImageParams(bitsPerComponent, csMode);
			if (csMode && *csMode != streamCSDeviceCMYK) {
				non_cmyk_stream += 1;
			}
			StreamKind kind = str->getKind();
			if (kind == strDCT) { /* jpg */
				// TODO check jpg color space
			}
		}

		void checkGfxImageColorMap(GfxImageColorMap *colorMap) {
			GfxColorSpace *space = colorMap->getColorSpace();
			if (space) {
				int mode = space->getMode();
				image_csm_counts[mode] += 1;
				if (csDeviceRGB == mode || csCalRGB == mode) {
					fail("RGB image on page %d", pageNum);
				}
			}
		}

		void checkGfxState(GfxState *state) {
			GfxColorSpace *fill = state->getFillColorSpace();
			if (fill) {
				fill_csm_counts[fill->getMode()] += 1;
			}
			GfxColorSpace *stroke = state->getStrokeColorSpace();
			if (stroke) {
				stroke_csm_counts[stroke->getMode()] += 1;
			}
		}
};

/** check that all fonts are embedded */
static void check_font_embedding(PDFDoc *doc)
{
	FontInfoScanner scanner(doc, 0);
	GooList *fonts = scanner.scan(doc->getNumPages());

	if (fonts) {
		for (int i = 0; i < fonts->getLength(); ++i) {
			FontInfo *font = (FontInfo *)fonts->get(i);
			if (font->getEmbedded()) continue;
			fail("Font not embedded: %-16s (%s)", font->getName() ? font->getName()->getCString() : "[none]", fontTypeNames[font->getType()]);
			delete font;
		}
		delete fonts;
	}
}

/** use the page render mechanism to perform perflight checks */
static void check_page_rendering(PDFDoc *doc, const int resolution)
{
	ValidatingOutputDev *valOut = new ValidatingOutputDev();
	int max = doc->getNumPages();
	for (int i=1; i <= max; i++) {
		Page *page = doc->getPage(i);
		valOut->setPageNum(i);
		assert (NULL != page);
		page->display(valOut,resolution,resolution,0,gTrue,gFalse,gTrue);
	}
	valOut->printReport();
	delete valOut;
}

int main(int argc, char *argv[])
{
	char *owner_password;
	char *user_password;
	int resolution = 300 /* DPI */;
	char **filenames = (char **) alloca(argc+1 * sizeof(char*));
	int i;
	for (i=0; i<argc+1; ++i) {
		filenames[i] = NULL;
	}

	optparse parser;
	optparse_init(&parser, VERSION);
	optparse_add_int_option(&parser, 'r', "resolution", &resolution, "print resolution in DPI");
	optparse_add_string_option(&parser, 0, "owner-password", &owner_password, "owner password");
	optparse_add_string_option(&parser, 0, "user-password", &user_password, "user password");
	optparse_parse(&parser, argc, argv, &filenames);

	// read poppler config file
	globalParams = new GlobalParams();

	i=1; /* filenames[0] is program name */
	while (filenames[i] != NULL) {
		GooString fileName(filenames[i]);
		GooString ownerPW(owner_password);
		GooString userPW(user_password);

		// open PDF file
		printf("check %s\n", filenames[i]);
		PDFDoc *doc = PDFDocFactory().createPDFDoc(fileName, &ownerPW, &userPW);

		if (!doc->isOk()) {
			error("Document %s broken!\n", filenames[i]);
		} else {
			check_page_rendering(doc, resolution);
			check_font_embedding(doc);
		}

		delete doc;
		i++;
	}

	delete globalParams;
	return 0;
}

// vim: noexpandtab
